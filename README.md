# trivia-app
This trivia app uses the [Open Trivia Database](https://opentdb.com) to fetch questions and categories. 

## Home
Welcome screen for the trivia app.
## Menu
Allows you to choose your desired trivia category, type, difficulty and the amount of questions.

## Game
Displays the questions and answers in a card format

## Score screen
Shows the results of the trivia and the final score. You can start a new game from this screen.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
