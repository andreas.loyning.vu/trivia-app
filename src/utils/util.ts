import { Question } from "@/interfaces/question";

export function shuffleAnswers(question: Question) {
  const answers = [...question.incorrectAnswers, question.correctAnswer];
  let currentIndex = answers.length,
    temporaryValue,
    randomIndex;
  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = answers[currentIndex];
    answers[currentIndex] = answers[randomIndex];
    answers[randomIndex] = temporaryValue;
  }

  return answers;
}

export function toCamel(s: string) {
  return s.replace(/([-_][a-z])/gi, $1 => {
    return $1
      .toUpperCase()
      .replace("-", "")
      .replace("_", "");
  });
}

export function keysToCamel(o: any): any {
  if (o === Object(o) && !Array.isArray(o) && typeof o !== "function") {
    const n: any = {};
    Object.keys(o).forEach(k => {
      n[toCamel(k)] = keysToCamel(o[k]);
    });
    return n;
  } else if (Array.isArray(o)) {
    return o.map(i => {
      return keysToCamel(i);
    });
  }
  return o;
}
