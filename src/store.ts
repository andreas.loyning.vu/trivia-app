import Vue from "vue";
import Vuex from "vuex";
import { Question, Questions } from "./interfaces/question";
import { TriviaCategories } from "./interfaces/category";

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    questions: [] as Questions,
    triviaCategoies: [] as TriviaCategories,
    score: 0,
    correctResults: [] as string[],
    wrongResults: [] as string[]
  },
  mutations: {
    // Used to set state component "questions" with questions
    // received from the API
    setQuestions(state, data: Questions) {
      state.questions = data;
    },
    setCategories(state, data: TriviaCategories) {
      state.triviaCategoies = data;
    },
    incrementScore(state) {
      state.score++;
    },
    setScore(state, data) {
      state.score = data;
    },
    setCorrectResults(state, data){
      state.correctResults.push(data);
    },
    setWrongResults(state, data){
      state.wrongResults.push(data);
    }
  },
  getters: {
    //Used to retrieve questions from the global state
    getQuestions: state => {
      return state.questions;
    },
    getScore: state => {
      return state.score;
    }, 
    getCorrectResults: state => {
      return state.correctResults;
    }, 
    getWrongResults: state => {
      return state.wrongResults;
    }
  },
  actions: {
    reset() {
      this.commit("setQuestions", []);
      this.commit("setScore", 0);
      this.state.correctResults = []
      this.state.wrongResults = []
    }
  }
});
export default store;
