export const difficulties = [
  {
    id: 1,
    level: "Easy"
  },
  {
    id: 2,
    level: "Medium"
  },
  {
    id: 3,
    level: "Hard"
  }
];

export const triviaTypes = [
  {
    id: 1,
    type: "",
    text: "Any"
  },
  {
    id: 2,
    type: "multiple",
    text: "Multiple Choice"
  },
  {
    id: 3,
    type: "boolean",
    text: "True / False"
  }
];

export const responseCodes = [
  {
    code: 0,
    output: "Success. Results returned successfully"
  },
  {
    code: 1,
    output: "No Results. The API does not have enough questions for your query"
  },
  {
    code: 2,
    output: "Invalid Parameter"
  },
  {
    code: 3,
    output: "Token Not Found. Please Create Session Token"
  },
  {
    code: 4,
    output:
      "Token Empty. Token has returned all possible questions for the selected options. Please reset Token"
  }
];
