export interface Question {
  category: string;
  type: QuestionType;
  difficulty: Difficulty;
  question: string;
  correctAnswer: string;
  incorrectAnswers: string[];
}

export type Questions = Array<Question>;

export enum Difficulty {
  Hard = "hard",
  Medium = "medium",
  Easy = "easy"
}

export enum QuestionType {
  Boolean = "boolean",
  Multiple = "multiple"
}
