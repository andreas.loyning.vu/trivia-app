export interface TriviaCategory {
  id: number;
  name: string;
}

export type TriviaCategories = Array<TriviaCategory>;

export interface QuestionsParams {
  amount: number;
  category: number;
  difficulty: string;
  type: string;
}
