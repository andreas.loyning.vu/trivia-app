import { Questions } from "@/interfaces/question";
import { keysToCamel } from "@/utils/util";
import axios from "axios";

const BASE_URL = "https://opentdb.com";

const baseApi = axios.create({
  baseURL: BASE_URL
});
function handleError(e: Error) {
  console.error(e.message);
}
export async function getCategories() {
  return await baseApi
    .get("/api_category.php")
    .then(response => {
      return response.data.trivia_categories;
    })
    .catch(handleError);
}

export async function getGameQuestions(params: any) {
  const responseData = await baseApi.get("/api.php", { params });
  switch (responseData.data.response_code) {
    case 0:
      return keysToCamel(responseData.data.results) as Questions;
    case 1:
      return "No Results. The API does not have enough questions for your query";
    case 2:
      return "Invalid Parameter";
    case 3:
      return "Token Not Found. Please Create Session Token";
    case 4:
      return "Token Empty. Token has returned all possible questions for the selected options. Please reset Token";
    default:
      return "Unknown error";
  }
}

export async function apiToken(params: any) {
  const response = await baseApi.get("/api_token.php", { params });
  return response.data;
}
